
# kalpra

<!-- badges: start -->
<!-- badges: end -->

The goal of *kalpra* (kernel approach for longitudinal pathway regression analysis) is to perform a pathway analysis with kernel machine regression analysis. Here, a pathway is tested for association with a longitudinal phenotype integrating network information. The package also enables analyzing a cross-sectional binary or cross-sectional continuous phenotype. Three kernels are available: linear kernel, network-based kernel and quadratic kernel. The genotype matrix cannot contain any missing data.

## Installation

You can install the package *kalpra* from GitLab.

``` r
devtools::install_git("https://gitlab.gwdg.de/bernadette.wendel/kalpra")
```

## Example

If we have a longitudinal phenotype with *m=4* measurement points and no missing data (example data set of package), we can perform a longitudinal pathway analysis in which a linear kernel is applied as following:

``` r
library(kalpra)

# Transform genotype matrix (no missing genotypes) from wide to long format
longGenotype<-makeLongGenotype(Genotypematrix, m=4)
dim(longGenotype)

# Selection of kernel, here: linear kernel
Kernellong<-Linearkernel(longGenotype)
dim(Kernellong)

# Perform linear machine regression analysis and obtain results for association test
longcompleteKMR<-KMR.Long(fixedEff = pheno~Age+Gender+time, randomeffects = ~1+time|IID,
                          phenotypedata = LongPhenotype,kernel = Kernellong,
                          method = "all",estimation = "REML")
summaryKMR(longcompleteKMR)

```

To perform a pathway analysis with network information (network kernel) and/or missing phenotype data see manual of *kalpra* and/or "workflow.pdf". The latter contains a flowchart explaining the single steps which need to be executed. 
